package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

var (
	playlist   string
	syncFolder string
)

func main() {
	linuxDataLocation := "/home/stephen/data/"
	windowsDataLocation := "D:\\"

	parseFlags()

	playlistData, err := os.ReadFile(playlist)
	if err != nil {
		log.Fatal(err)
	}

	playlistFiles := strings.Split(string(playlistData), "\r\n")

	newPlaylistFiles := make([]string, 0)

	for _, file := range playlistFiles {
		if file == "" {
			continue
		}
		linuxVariant := strings.Replace(file, windowsDataLocation, linuxDataLocation, 1)
		linuxVariant = strings.ReplaceAll(linuxVariant, "\\", "/")

		outputLocation := strings.Replace(linuxVariant, linuxDataLocation, syncFolder+"/", 1)
		outputPlaylistLocation := strings.Replace(linuxVariant, linuxDataLocation, "", 1)

		outputFolders := outputLocation[:strings.LastIndex(outputLocation, "/")]
		err = os.MkdirAll(outputFolders, 0755)
		if err != nil {
			log.Fatal(err)
		}

		fileSource, err := os.Open(linuxVariant)
		if err != nil {
			log.Fatal(err)
		}

		fileDestination, err := os.Create(outputLocation)
		if err != nil {
			log.Fatal(err)
		}

		_, err = io.Copy(fileDestination, fileSource)
		if err != nil {
			log.Fatal(err)
		}

		err = fileSource.Close()
		if err != nil {
			log.Fatal(err)
		}

		newPlaylistFiles = append(newPlaylistFiles, outputPlaylistLocation)
	}

	playlistComponents := strings.Split(playlist, "/")
	playlistFilename := playlistComponents[len(playlistComponents)-1]
	newPlaylistFilename := fmt.Sprintf("%s/%s", syncFolder, playlistFilename)
	log.Println(playlistFilename)
	log.Println(newPlaylistFilename)
	err = os.WriteFile(newPlaylistFilename, []byte(strings.Join(newPlaylistFiles, "\n")), 0755)
	if err != nil {
		log.Fatal(err)
	}
}

func parseFlags() {
	flag.StringVar(&playlist, "playlist", "aaa.m3u", "the location of the playlist to convert")
	flag.StringVar(&syncFolder, "sync-folder", "aaa", "the folder where stuff is synced from PC to phone")
	flag.Parse()
}
