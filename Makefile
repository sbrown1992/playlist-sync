all:
	make verify
	go build -o playlist-sync
	./playlist-sync --playlist=weezersetlist.m3u --sync-folder=fake-android

verify:
	go fmt ./...
	go vet ./...
	go test ./...
	go mod tidy

weezer:
	go build -o playlist-sync
	./playlist-sync --playlist=/home/stephen/data/Playlists/weezersetlist.m3u --sync-folder=/home/stephen/data/AndroidMusicSync

coolstuff:
	go build -o playlist-sync
	./playlist-sync --playlist=/home/stephen/data/Playlists/coolstuff.m3u --sync-folder=/home/stephen/data/AndroidMusicSync
